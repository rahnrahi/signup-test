import React from 'react'
import axios from 'axios'
import {Container, Row, Col, Form, Button} from 'react-bootstrap'
import '../signup.css'
let interval 
class Signup extends React.Component {
    constructor(props){
      super(props)
      this.state = {
          firstName: '',
          lasttName: '',
          userName: '',
          passWord: '',
          isValidusername: false,
          errors:{touched: true}
      }
    }

    checkPassWord(){
        const  { passWord} = this.state  
        let error = false
        if (passWord.length < 8) {
            error = true;
          }
          if (passWord.search(/[a-z]/) === -1) {
            error = true;
          }
          if (passWord.search(/[A-Z]/) === -1) {
            error = true;
          }
          if (passWord.search (/[0-9]/) === -1) {
            error = true;
          }
          return error
    }

    async checkErrors(key){
      const  {firstName, lasttName, userName} = this.state  
      let errors = {...this.state.errors}
      delete errors['touched']; 
      //console.log("....errors", errors)
      if(firstName==='' && key==='firstName'){
        errors['firstNameErr'] = 'First name is required'
      }else if(key==='firstName'){
        delete  errors['firstNameErr']
      }

      if(lasttName==='' && key==='lasttName'){
        errors['lasttNameErr'] = 'Last name is required'
      }else if(key==='lasttName'){
        delete  errors['lasttNameErr']
      }

      if(userName==='' && key==='userName'){
        errors['userNameErr'] = 'User name is required'
      }else if(key==='userName'){
        delete  errors['userNameErr']
      }

      if(key==='passWord' && this.checkPassWord()){
        errors['passWordErr'] = 'Password Must be 8 characters long and contain atleast one number, one upper case and one lower case character'
      }else if(key==='passWord'){
        delete  errors['passWordErr']
      }

      if(key==='userName'){
        let userNameValid = await this.checkusername()
        if(!userNameValid){
          errors['userNameErr'] = 'Username is not available'
        }else{
          delete  errors['userNameErr']
        }
      }
     
      this.setState({errors})
    }

    chaneValue(e){
      let prevState  = {...this.state}
      prevState[e.target.name] = e.target.value
      let self = this
      this.setState(prevState,()=>{
          if(e.target.name==='userName'){
            if(interval){
                clearTimeout(interval)
            }    
            interval = setTimeout(()=>{
                self.checkErrors(e.target.name)
            }, 400)  
          }else{
            self.checkErrors(e.target.name)
          }
      })
    }

    checkusername(){
        let self  =  this;
        return new Promise((res, rej)=>{
            axios.get('https://www.reddit.com/api/username_available.json', {
                params: {
                    user: this.state.userName
                }
              })
              .then(function (response) {
                 console.log("...response", response.data) 
                 if(response.data===true){
                    self.setState({isValidusername: true})
                    res(true)
                 }else{
                    self.setState({isValidusername: false})
                    res(false)
                 }
              })
        })
        
    }

    SignUp(){
        let err = Object.entries(this.state.errors)
        if(err.length){
            window.alert("Invalid form data")
        }else{
            window.alert("Valid form data")
        }
    }

    render() { 

        const  {firstName, lasttName, userName, passWord, errors} = this.state
        const {firstNameErr, lasttNameErr, userNameErr, passWordErr, touched} = errors
        console.log(Object.entries(errors), touched)
        return (
            <Container>
                <Row id="register">
                    <Col className="register-left"  sm={3}>
                        <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                        <h3>Welcome</h3>
                        <p>You are 30 seconds away from earning your own money!</p>
                        <input type="submit" name="" value="Login"/><br/>
                    </Col>
                    <Col sm={9} className="register-right">
                      <Form className="register-form">
                           <h2 className="register-heading">Sign up Now!</h2>

                          <Row>
                              <Col>
                                <Form.Group className="mb-3" controlId="formBasicEmail">
                                  <Form.Label>First Name</Form.Label>
                                  <Form.Control value={firstName} name="firstName" onChange={(e)=>this.chaneValue(e)} type="text" placeholder="Enter First Name" />
                                  <Form.Text className="text-muted">
                                      {firstNameErr && (<p>{firstNameErr}</p>)}
                                  </Form.Text>
                              </Form.Group>
                              </Col>
                              <Col>
                                <Form.Group className="mb-3" controlId="formBasicEmail">
                                  <Form.Label>Last Name</Form.Label>
                                  <Form.Control value={lasttName}  name="lasttName"  onChange={(e)=>this.chaneValue(e)} type="text" placeholder="Enter Lirst Name" />
                                  <Form.Text className="text-muted">
                                    {lasttNameErr && (<p>{lasttNameErr}</p>)}
                                  </Form.Text>
                              </Form.Group>
                              </Col>
                          </Row>
                           
                          <Row>
                              <Col>
                                <Form.Group className="mb-3" controlId="formBasicuserName">
                                    <Form.Label>User Name</Form.Label>
                                    <Form.Control  value={userName} name="userName" onChange={(e)=>this.chaneValue(e)}  type="text" placeholder="Enter User Name" />
                                    <Form.Text className="text-muted">
                                      {userNameErr && (<p>{userNameErr}</p>)}
                                    </Form.Text>
                                </Form.Group>
                              </Col>
                              <Col>
                                <Form.Group className="mb-3" controlId="formBasicPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control value={passWord} name="passWord" onChange={(e)=>this.chaneValue(e)} type="password" placeholder="Password" />
                                    <Form.Text className="text-muted">
                                      {passWordErr && (<p>{passWordErr}</p>)}
                                      
                                    </Form.Text>
                                </Form.Group>
                              </Col>
                          </Row> 

                           
                          <Row>
                              <Col> 
                                 &nbsp;
                              </Col>
                              <Col> 
                                  <Button variant="primary" className="btnRegister"  onClick={()=>this.SignUp()} type="button">
                                    Register
                                  </Button>
                              </Col>
                          </Row>    
                           

                           
                        
                            
                    </Form>
                    </Col>
                </Row>
            </Container>
        );
    }
}
 
export default Signup;