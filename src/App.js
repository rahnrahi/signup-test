import 'bootstrap/dist/css/bootstrap.min.css';

import Signup from './components/SignUp';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Signup/>
      </header>
    </div>
  );
}

export default App;
